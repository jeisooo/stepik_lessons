from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import time
import math
from selenium.webdriver.support.ui import Select

def calc(x):
  return str(math.log(abs(12*math.sin(int(x)))))
def test(link):
    try: 
        
        browser = webdriver.Chrome()
        browser.get(link)

        browser.implicitly_wait(5)
        WebDriverWait(browser, 12).until(EC.text_to_be_present_in_element((By.ID, "price"),"$100"))
        button1 = browser.find_element_by_id("book")
        button1.click()
        
        x_element = browser.find_element_by_id("input_value").text
        input_x = browser.find_element_by_id("answer")
        answer = calc(x_element)
        input_x.send_keys(answer)
        button = browser.find_element_by_id("solve")

        #assert "successful" in message.text
        button.click()

    finally:
        # ожидание чтобы визуально оценить результаты прохождения скрипта
        time.sleep(20)
        # закрываем браузер после всех манипуляций
        browser.quit()
        #return "OK"

link = "http://suninjuly.github.io/explicit_wait2.html" 
test(link)


