from selenium import webdriver
from selenium.webdriver.common.by import By
import time
import math
from selenium.webdriver.support.ui import Select

def calc(x):
  return str(math.log(abs(12*math.sin(int(x)))))
def test(link):
    try: 
        
        browser = webdriver.Chrome()
        browser.get(link)

        # Ваш код, который заполняет обязательные поля
        x_element = browser.find_element_by_id("num1").text
        x_element2 = browser.find_element_by_id("num2").text
        sum1 = int(x_element) + int(x_element2)
        select = Select(browser.find_element_by_tag_name("select"))
        select.select_by_value(str(sum1)) #.click()

        # Отправляем заполненную форму
        button = browser.find_element_by_tag_name("button")
        button.click()

        # Проверяем, что смогли зарегистрироваться
        # ждем загрузки страницы
        time.sleep(1)


    finally:
        # ожидание чтобы визуально оценить результаты прохождения скрипта
        time.sleep(5)
        # закрываем браузер после всех манипуляций
        browser.quit()
        #return "OK"

link = "http://suninjuly.github.io/selects1.html" 
test(link)


