from selenium import webdriver
from selenium.webdriver.common.by import By
import time
import math
from selenium.webdriver.support.ui import Select

def calc(x):
  return str(math.log(abs(12*math.sin(int(x)))))
def test(link):
    try: 
        
        browser = webdriver.Chrome()
        browser.get(link)

        button = browser.find_element_by_tag_name("button")
        button.click()

        alert = browser.switch_to.alert
        alert.accept()
        #assert True

        x_element = browser.find_element_by_id("input_value").text
        input_x = browser.find_element_by_id("answer")
        answer = calc(x_element)
        input_x.send_keys(answer)

        button = browser.find_element_by_tag_name("button")
        button.click()

        


    finally:
        # ожидание чтобы визуально оценить результаты прохождения скрипта
        time.sleep(10)
        # закрываем браузер после всех манипуляций
        browser.quit()
        #return "OK"

link = "http://suninjuly.github.io/alert_accept.html" 
test(link)


