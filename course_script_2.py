
import time

# webdriver это и есть набор команд для управления браузером
from selenium import webdriver

# инициализируем драйвер браузера. После этой команды вы должны увидеть новое открытое окно браузера
#driver = webdriver.Chrome()
try: 
    browser = webdriver.Chrome()
    browser.get("http://suninjuly.github.io/simple_form_find_task.html")
    button = browser.find_element_by_id("submit_button")
finally:
    browser.quit()
# После выполнения всех действий мы не должны забыть закрыть окно браузера
#driver.quit()
