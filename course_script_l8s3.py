from selenium import webdriver
from selenium.webdriver.common.by import By
import time
import math
from selenium.webdriver.support.ui import Select

def calc(x):
  return str(math.log(abs(12*math.sin(int(x)))))
def test(link):
    try: 
        
        browser = webdriver.Chrome()
        browser.get(link)

        browser.implicitly_wait(12)
        button = browser.find_element_by_id("verify")
        button.click()
        message = browser.find_element_by_id("verify_message")

        assert "successful" in message.text


    finally:
        # ожидание чтобы визуально оценить результаты прохождения скрипта
        time.sleep(10)
        # закрываем браузер после всех манипуляций
        browser.quit()
        #return "OK"

link = "http://suninjuly.github.io/explicit_wait2.html" 
test(link)


