from selenium import webdriver
from selenium.webdriver.common.by import By
import time
import math

def calc(x):
  return str(math.log(abs(12*math.sin(int(x)))))
def test(link):
    try: 
        
        browser = webdriver.Chrome()
        browser.get(link)

        # Ваш код, который заполняет обязательные поля
        x_element = browser.find_element_by_id("input_value")
        x = x_element.text
        y = calc(x)
        input1 = browser.find_element_by_id("answer")
        input1.send_keys(y)
        checkbox1 = browser.find_element_by_id("robotCheckbox")
        checkbox1.click()
        radiobutton1 = browser.find_element_by_id("robotsRule")
        radiobutton1.click()

        # Отправляем заполненную форму
        button = browser.find_element_by_css_selector("button")
        button.click()

        # Проверяем, что смогли зарегистрироваться
        # ждем загрузки страницы
        time.sleep(1)


    finally:
        # ожидание чтобы визуально оценить результаты прохождения скрипта
        time.sleep(10)
        # закрываем браузер после всех манипуляций
        browser.quit()
        #return "OK"

link = "http://suninjuly.github.io/math.html" 
test(link)


